touch


Updates access and modification time stamps of your file. If it doesn't exists, it'll be created.

touch filename

Example:

$ touch trick.md

touch

Aggiorna l'accesso e modifica i timestamp del tuo file. Se non esiste, verr� creato.
touch il nome file
Esempio:
$ touch trick.md




sed

Stream editor for filtering and transforming text
example.txt
Hello This is a Test 1 2 3 4
replace all spaces with hyphens
sed 's/ /-/g' example.txt
Hello-This-is-a-Test-1-2-3-4
replace all digits with "d"
sed 's/[0-9]/d/g' example.txt
Hello This is a Test d d d d


sed

Editor di stream per filtrare e trasformare il testo
example.txt
Ciao, questo � un test 1 2 3 4
sostituisci tutti gli spazi con trattini
sed 's / / - / g' example.txt
Ciao-this-is-a-Test-1-2-3-4
sostituire tutte le cifre con "d"
sed 's / [0-9] / d / g' example.txt
Ciao, questo � un test d d d d






 sort

Sort lines of text files
example.txt
f
b
c
g
a
e
d
sort example.txt
sort example.txt
a
b
c
d
e
f
g
randomize a sorted example.txt
sort example.txt | sort -R
b
f
a
c
d
g
e



sort

Ordina le righe dei file di testo
example.txt
f
B
c
g
un'
e
d
sort example.txt
sort example.txt
un'
B
c
d
e
f
g
randomizza un esempio.txt ordinato
sort example.txt | ordina -R
B
f
un'
c
d
g
e


